class Sample < ApplicationRecord
  belongs_to :user
  has_one_attached :audio 
  validates :title, presence: true
  validate :sample_type
  public
  def audio_on_disk
    ActiveStorage::Blob.service.send(:path_for, audio.key)
  end
  
  private
  def sample_type
    if audio.attached? == false
      errors.add(:audio, "Select a wav file")
    end
    
  if audio.attached? == true
    if !audio.content_type.in?(%('audio/x-wav'))
      errors.add(:audio, "needs to be a wav file")
    end
   end
  end
end

