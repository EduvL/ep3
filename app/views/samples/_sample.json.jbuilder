json.extract! sample, :id, :title, :user_id, :created_at, :updated_at
json.url sample_url(sample, format: :json)
