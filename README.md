﻿![top](https://gitlab.com/EduVL/ep3/uploads/e503bfe70be74c4a5ed4e54e909ca639/logo.png)

[minim]: https://gitlab.com/EduvL/ep3/uploads/77f872e825a1b4ba3194dfa9ae3f26a4/minim.png
[crotchet]: https://gitlab.com/EduvL/ep3/uploads/b92cfe0502064119aa5b02200b778104/crotchet.png
[quaver]: https://gitlab.com/EduvL/ep3/uploads/c80fc610c3ec81a5c9f6b92284553556/quaver.png
[semiquaver]: https://gitlab.com/EduvL/ep3/uploads/7e91c578837341ec2dd6263c8f0fb851/semiquaver.png
[doublecrotchet]: https://gitlab.com/EduvL/ep3/uploads/42335906f3fff5ed433f7ca05d0dbd20/doublecrotchet.png

kTunes, the sampling keyboard that delivers a fun and unique musical experience.
## ![minim]. Summary
The kTunes sampling keyboard has the following functionalities:

 - Play the usual piano sounds, like most keyboards.
 - Use the user imported samples as the keyboard notes. 
 - Allow the users to register and manage their samples.
 
It is highly recommended for you to read the README file or, at the very least, the Dependencies and Possible bugs, problems or issues section.<br/>
Some features, optional or not, are yet to be implemented from this commit. Those are:
 - Export the user sample from the database for the keyboard use.

## ![crotchet]. Dependencies
kTunes is a Ruby on Rails web application and, for that reason, is dependant on various gems, so it is of extreme relevance that anyone who plans to host the application to have all the gems installed, otherwise the application may present malfunctionalities or not work at all. <br/>
All the used gems can be found in the Gemfile file, but here also follows the list of all used gems, including default gems:
 - devise;
 - jquery-rails;
 - rails;
 - sqlite3;
 - puma;
 - sass-rails;
 - uglifier;
 - coffee-rails;
 - turbolinks;
 - jbuilder;
 - bootsnap;
 - byebug (development, test);
 - web-console (development);
 - listen (development);
 - spring (development);
 - spring-watcher-listen (development);
 - capybara (test);
 - selenium-webdriver (test);
 - chromedriver-helper (test);
 - tzinfo-data (test).<br/>
The kTunes keyboard is also dependant on the [Rubber Band C++ library](https://breakfastquay.com/rubberband/index.html). For that reason, a **LINUX** binary file, by the name rubberband.linux, is included in following path: app/assets/rubberband. If you're using a different OS, such as macOS or Windows, or an incompatible Linux distribution, access the [Rubber Band Library download page](https://breakfastquay.com/rubberband/index.html) and download the respective binary for your system or generate the binary through the source code, and then replace the rubberband.linux file with your binary.
## ![crotchet].	How to use
### ![doublecrotchet]. Homepage
By entering the kTunes web application, expecting that you started by the homepage, the user will be greeted by the following page:
![1](https://gitlab.com/EduvL/ep3/uploads/aefda2e3ed7cbacb8ff469332c8d31a6/1.png)</br>
This is the homepage and in here we have the main functionality of the application, the keyboard.<br/>
The keyboard can be played through two ways: first, by clicking each individual keyboard key with their mouse; second, by using their computer keyboard keys to play, being the first key (C1) the '8' key and the last one (B3) the 'k' key. In the computer keyboard, to play the sustained notes, also known as black keys, the user needs to either hold the Shift key or activate Caps Lock, and then press the correspondent key.
### ![doublecrotchet]. Register page
By going to the navigation menu, right below the kTunes logo, and clicking 'Register', the user will be brought to the following page:
![2](https://gitlab.com/EduvL/ep3/uploads/29908ea8add508832bea8482b815a773/2.png)<br/>
Here, the user will be able to create their account, by inputting their e-mail address, a password and confirming their password, and then pressing the Sign up button.<br/>
To register, the user information needs to fit the following conditions:
 - A valid e-mail address was entered;
 - The password has a minimum of 6 characters;
 - The password and password confirmation match.
### ![doublecrotchet]. Login page
By clicking 'Sign in' in the navigation menu or Log in in the Register page, the user will be redirected to the following page:
![3](https://gitlab.com/EduvL/ep3/uploads/9ddeb2dcbf0e16bf2fef4cb167daa991/3.png)<br/>
In this page, the user will be able to login to their account, if their input a valid account information. The user is also allowed to check the 'Remember me' checkbox to remove the need to Log in after every access.</br>
The user can also request to recover their account's password by clicking on the 'Forgot your password?' below the Log in button. After doing so, the page will look like this:
![4](https://gitlab.com/EduvL/ep3/uploads/93938aed1e00254e121fc1024163d5e4/4.png)<br/>
Here, the user will input a valid e-mail to receive the account recovering instructions.<br/>
After logging in, the user will be redirected to the homepage and new options will appear in the Navigation Menu. Here is how it will look like:
![5](https://gitlab.com/EduvL/ep3/uploads/fdeb1ddcfb7e1730420087541297a7ee/5.png)<br/>
By clicking in the 'Sign out', the user will simply be logged out of their account.
### ![doublecrotchet]. Samples page
After cliking in the 'Samples' in the navigation menu, the user will be brought to the following page:
![6](https://gitlab.com/EduvL/ep3/uploads/741cf968ada24419ef08da5067d89b08/6.png)<br/>
The user will be able to upload a sample by clicking on 'New Sample'. By doing so, the page will look like this:
![7](https://gitlab.com/EduvL/ep3/uploads/1e91e1660a3526f7deddbfd455080ee2/7.png)<br/>
Here the user will be able to upload an **AUDIO** file, where a title **MUST** be inputted. The supported format for audio files is **.wav**.<br/>
After uploading a sample, the user be shown the uploaded sample page, where they can download or edit it. By clicking on back, here is how it should look like using an example sample called "Gnome":
![8](https://gitlab.com/EduvL/ep3/uploads/1a9e0578ba4ff212cb45e86474525b27/8.png)<br/>
For each uploaded sample the user can choose to check the sample page by clicking on Show, to edit it by clicking on Edit or to delete it by clicking on Destroy.<br/>
After selecting a sample, the user should be redirected to this loading page:
![9](https://gitlab.com/EduvL/ep3/uploads/25409836998cbec0ef68daf9d1ef495c/9.png)</br>
After some seconds, the user will be redirected to the homepage and will be able to play using their sample.
## ![quaver].	Possible bugs, problems or issues
Known possible bugs, problems or issues are as follows:
 - Although it is probably a Ruby on Rails procedure, it is worth mentioning that it seems to be necessary to use the 'rake db:migrate' command in your terminal before booting up your Rails server.
 - It has been observed once for the sample loading to fail after the first attempt time booting up the server and trying to load the sample. It is not yet known if the problems persists for it was an one-time only occurrence.

## ![semiquaver].	References
 - [Desenvolvimento Ágil para Web com Ruby on Rails - Caelum](https://www.caelum.com.br/apostila-ruby-on-rails/);
 - [Documenting the Ruby Language](https://ruby-doc.org/).
