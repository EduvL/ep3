Rails.application.routes.draw do
  resources :samples
  devise_for :users
	root 'pages#homepage', as: 'homepage'
  get '/loading' => 'pages#loading', as: 'loading'
  get '/howto' => 'pages#howto', as: 'howto'
 
end
